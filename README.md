# pinky-01
This is the pinky pig #01

## Description
Small API that returns flat information about a product, based on its ID. The backing of this API is a SQLite DB, which is present in this repo.

## Run notes
Node version: 6.9.x
The solution requires redis.

## Improvements
* Develop a caching technique to avoid doing the multiple queries to the DB
  > Uses redis to cache the return data.
* Actually implement the locale feature
  > Add locale (only numerical values) with en and pt formats.
  > Return product in the correct language (or not found).
* Unit testing
  > _npm test_ to run the tests.
  > Uses jest and supertest.
