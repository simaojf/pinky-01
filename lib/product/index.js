'use strict';

const _       = require('lodash'),
      sqlite3 = require('sqlite3'),
      db      = new sqlite3.Database('prod.db'),
      redis   = require('redis'),
      rClient = redis.createClient(),
      localize = require('../locale');

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function checkAndGetName(product) {
    return new Promise((resolve, reject) => {
        if (!isNumeric(product.id)) {
            return reject(new Error('Not a valid ID'));
        }

        db.get('SELECT name FROM product WHERE id = $id', {
            $id: product.id
        }, function(err, rows) {
            if (err) {
                return reject(err);
            }

            if (_.isEmpty(rows)) {
                return resolve(false);
            }

            resolve(rows);
        });
    });
}

function getProductInfo(rows, product, countryCode) {
    return new Promise((resolve, reject) => {
        if (rows) {
            product.name = rows.name;
            db.get('SELECT description, stock, price FROM product_info pi, product_stock ps WHERE pi.id = $id AND pi.locale = $code AND ps.id = $id', {
                $id: product.id,
                $code: countryCode
            }, function(err, rows) {
                if (err) {
                    return reject(err);
                }

                if (!rows) {
                    reject(404);
                    return;
                }

                product.description = rows.description;
                product.stock = rows.stock;
                product.price = rows.price;

                // Set product on memory (redis).
                setProductMemory(product.id, countryCode, JSON.stringify(product));

                resolve(product);
            });
        } else {
            reject(404);
        }
    });
}

function handleError(err, res) {
    if (err === 404) {
        res.status(404).json({
            message: "Product not found"
        });
    } else {
        res.status(500).json({
            message: err.message
        });
    }
}


function getProductMemory(pid, countryCode) {
    var productKey = 'product_' + pid + '_' +countryCode;

    return new Promise((resolve, reject) => {
        rClient.get(productKey, function (err, reply) {
            if (reply) {
                return resolve(JSON.parse(reply));
            }
                return reject();
        });
    });
}

function setProductMemory(pid, countryCode, value, ttl) {
    var productKey = 'product_' + pid + '_' +countryCode;

    if (ttl && Number.isInteger(ttl)) {
        rClient.set(productKey, value, 'EXP', ttl);
    } else {
        rClient.set(productKey, value);
    }
}

function getFlatProduct(req, res) {
    const locale = req.params.locale || 'en';

    let product = {
        id: req.params.pid
    };

    // Try to retrieve the product from memory. If fails, retrieve it from database.
    getProductMemory(product.id, locale).then((reply) => {
        reply.price = localize(locale, reply.price);
        res.json(reply);
    }).catch(() => {
        checkAndGetName(product).then((rows) => {
            return getProductInfo(rows, product, locale);
        }).then(() => {
            product.price = localize(locale, product.price);
            res.json(product);
        }).catch((err) => {
            handleError(err, res);
        });
    });

}

function Product() {
    return function(req, res) {
        getFlatProduct(req, res);
    };
}

module.exports = Product;
