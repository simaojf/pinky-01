module.exports = {
    DECIMAL_SEPARATOR: '.',
    THOUSAND_SEPARATOR: ',',
    NUMBER_GROUP: 3
};