
function getLocal(countryCode) {
    try {
        return require('./formats/' + countryCode);
    } catch (e) {
        return null
    }

}

function parseValue(format, value) {
    var sign = '';
    if (value < 0) {
        sign = '-';
        value = Math.abs(value)
    }

    var valueParts = value.toString().split('.');
    var decimalPart = valueParts[1] ? format.DECIMAL_SEPARATOR + valueParts[1] : '';

    var tempInt = '';
    var invertedIntPart = valueParts[0].split('').reverse().join('');
    for (var i=0; i < invertedIntPart.length; i++) {
        if (i !== 0 && i % format.NUMBER_GROUP === 0) {
            tempInt += format.THOUSAND_SEPARATOR;
        }
        tempInt += invertedIntPart[i];
    }

    var intPart = tempInt.split('').reverse().join('');

    return sign + intPart + decimalPart;
}

function localize(countryCode, value) {
    var format = getLocal(countryCode);

    if (format) {
        return parseValue(format, value);
    } else {
        console.log(`No locale ${countryCode}`);
        return value
    }
}

module.exports = localize;
