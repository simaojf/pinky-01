const localize = require('../lib/locale');

describe('Test locale.', () => {
    it('should turn 9999.99 value into 9,999.99 using en locale', () => {
        expect(localize('en', 9999.99)).toBe('9,999.99');
    });

    it('should turn the negative value -9999.99 into -9,999.99 using en locale', () => {
        expect(localize('en', -9999.99)).toBe('-9,999.99');
    });

    it('should turn 9999.99 value into 9.999,99 using pt locale', () => {
        expect(localize('pt', 9999.99)).toBe('9.999,99');
    });

    it('should turn 100.01 value into 100,01 using pt locale', () => {
        expect(localize('pt', 100.01)).toBe('100,01');
    });

    it('should not change 100 value', () => {
        expect(localize('pt', 100)).toBe('100');
    });

    it('should not change 9999.99 value using a wrong country code', () => {
        expect(localize('some', 9999.99)).toBe(9999.99);
    });
});
