const request = require('supertest');
const express = require('express');
const product = require('../lib/product');
const redis   = require('redis');
const rTestClient = redis.createClient();
const app = require('../index');

rTestClient.flushdb( function (err, succeeded) {
    console.log('Redis flushed on init.');
});

describe('Flow API', () => {

    describe('GET /product/$id', () => {

        it('should return product not found', () => {
            return request(app).get('/product/0')
                .expect(404);
        });

        it('should return 500 error', () => {
            return request(app).get('/product/infinito')
                .expect(500);
        });

        it('should return json with correct values', () => {
            return request(app).get('/product/2')
                .expect(200)
                .then(res => {
                    expect(res.body.id).toBe('2');
                    expect(res.body.name).toBe('BMW M3');
                    expect(res.body.description).toBe('A faster way to run from the cops and hit a lamp post.');
                    expect(res.body.stock).toBe(1);
                    expect(res.body.price).toBe('120,000');
                });
        });

        it('should have the product 2 cached on redis', () => {
            expect(rTestClient.get('product_2_en')).toBe(true);
        });

        it('should return json with correct values from cache', () => {
            return request(app).get('/product/2')
                .expect(200)
                .then(res => {
                    expect(res.body.id).toBe('2');
                    expect(res.body.name).toBe('BMW M3');
                    expect(res.body.description).toBe('A faster way to run from the cops and hit a lamp post.');
                    expect(res.body.stock).toBe(1);
                    expect(res.body.price).toBe('120,000');
                });
        });

  });

});
